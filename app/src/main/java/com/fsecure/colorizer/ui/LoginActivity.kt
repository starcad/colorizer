package com.fsecure.colorizer.ui

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.example.awesomedialog.*
import com.fsecure.colorizer.App
import com.fsecure.colorizer.viewmodels.LoginViewModel
import com.fsecure.colorizer.R
import com.fsecure.colorizer.databinding.ActivityLoginBinding
import com.fsecure.colorizer.datamodels.LoginDataOut
import com.fsecure.colorizer.datamodels.LoginProcessStates

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel : LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)

        observeViewModel();

        binding.inputLogin.setText(viewModel.logData?.username)
        binding.inputPassword.setText(viewModel.logData?.password)

        binding.buttonLogin.setOnClickListener {
            if (loginDataValidate()) {
                enableForm(false)
                viewModel.login()
            }
        }

        binding.inputPassword.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                //ignore
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                binding.passwordLayout.isEndIconVisible = true
            }

            override fun afterTextChanged(editable: Editable) {
                //ignore
            }
        })
    }

    private fun enableForm(enablityTrigger: Boolean) {
        binding.inputLogin.isEnabled = enablityTrigger
        binding.inputPassword.isEnabled = enablityTrigger
        binding.buttonLogin.isEnabled = enablityTrigger
    }

    private fun startProgress() {

        binding.loadingProgressBar.showProgressBar()
        binding.loadingProgressBar.visibility = View.VISIBLE
    }

    private fun stopProgress() {
        binding.loadingProgressBar.hideProgressBar()
        binding.loadingProgressBar.visibility = View.INVISIBLE
    }

    private fun loginDataValidate(): Boolean {
        var valid = true

        if (binding.inputLogin.text == null || binding.inputLogin.text!!.isEmpty()) {
            binding.inputLogin.requestFocus()
            binding.inputLogin.error = getString(R.string.error_not_be_empty)
            valid = false
        } else if (binding.inputPassword.text == null || binding.inputPassword.text!!.isEmpty()) {
            binding.inputPassword.requestFocus()
            binding.passwordLayout.isEndIconVisible = false
            binding.inputPassword.error = getString(R.string.error_not_be_empty)
            valid = false
        } else {
            viewModel.logData = LoginDataOut(
                binding.inputLogin.text.toString(),
                binding.inputPassword.text.toString()
            )
        }

        return valid
    }

    private fun gotoNext() {
        val i = Intent(App.appContext, MainActivity::class.java)
        startActivity(i)
        finish()
    }

    private fun showErrorMessage(errorMessage: String)
    {
        AwesomeDialog.build(this)
            .title(getString(R.string.title_warning))
            .body(errorMessage)
            .icon(R.drawable.ic_error)
            .onPositive(getString(R.string.button_Ok)) {
                enableForm(true)
            }
    }

    private fun observeViewModel() {

        viewModel.loginProcessDataObservable?.observe(this, { loginResult ->
            when(loginResult.state)
            {
                LoginProcessStates.START -> startProgress()
                LoginProcessStates.ERROR -> {
                    stopProgress()
                    showErrorMessage(loginResult.message)
                }
                else -> gotoNext();
            }
        })
    }
}