package com.fsecure.colorizer.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.example.awesomedialog.*
import com.fsecure.colorizer.App
import com.fsecure.colorizer.R
import com.fsecure.colorizer.databinding.ActivitySplashscreenBinding
import com.fsecure.colorizer.datamodels.LoadColorProcessStates
import com.fsecure.colorizer.viewmodels.SplashscreenViewModel

class SplashscreenActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySplashscreenBinding
    private lateinit var viewModel : SplashscreenViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySplashscreenBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        viewModel = ViewModelProviders.of(this).get(SplashscreenViewModel::class.java)

        observeViewModel()

        when {
            viewModel.isAlreadyHaveStorage() -> {
                viewModel.getColorFromStorage()
            }
            viewModel.isAlreadyAuth() -> {
                gotoMain()
            }
            else -> {
                gotoLogin()
            }
        }
    }

    private fun startProgress() {

        binding.loadingProgressBar.showProgressBar()
        binding.loadingProgressBar.visibility = View.VISIBLE
    }

    private fun stopProgress() {
        binding.loadingProgressBar.hideProgressBar()
        binding.loadingProgressBar.visibility = View.INVISIBLE
    }

    private fun observeViewModel() {

        viewModel.process.observe(this, { colorLoadingResult ->
            when(colorLoadingResult.state) {
                LoadColorProcessStates.START -> startProgress()
                LoadColorProcessStates.ERROR -> {
                    stopProgress()
                    showErrorMessage(colorLoadingResult.message)
                }
                else -> gotoMain();
            }
        })
    }

    private fun showErrorMessage(errorMessage: String)
    {
        AwesomeDialog.build(this)
            .title(getString(R.string.title_warning))
            .body(errorMessage)
            .icon(R.drawable.ic_error)
            .onPositive(getString(R.string.button_Ok)) {
                viewModel.getColorFromStorage()
            }
    }

    private fun gotoMain()
    {
        val i = Intent(App.appContext, MainActivity::class.java)
        startActivity(i)
        finish()
    }

    private fun gotoLogin()
    {
        val i = Intent(App.appContext, LoginActivity::class.java)
        startActivity(i)
        finish()
    }
}