package com.fsecure.colorizer.ui

import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.example.awesomedialog.*
import com.fsecure.colorizer.App
import com.fsecure.colorizer.R
import com.fsecure.colorizer.databinding.ActivityMainBinding
import com.fsecure.colorizer.datamodels.LoadColorProcessStates
import com.fsecure.colorizer.models.ColorDataController
import com.fsecure.colorizer.viewmodels.MainViewModel


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel : MainViewModel

    lateinit var progressDialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        binding.colorPicker.subscribe { color, _, _ ->
            viewModel.selectedColor = color
            view.setBackgroundColor(viewModel.selectedColor)
        }

        binding.colorPicker.setInitialColor(viewModel.selectedColor)

        binding.buttonSaveColor.setOnClickListener {
           viewModel.saveColor()
        }

        val progressDialogB: AlertDialog.Builder = AlertDialog.Builder(this)
        progressDialogB.setView(R.layout.progress)
        progressDialog = progressDialogB.create()

        observeViewModel()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.business_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_logout -> {
                val builder = AlertDialog.Builder(this)
                builder.setTitle(resources.getString(R.string.title_dialog_exit)).setMessage(
                    resources.getString(
                        R.string.message_exit_confirmation
                    )
                )
                    .setCancelable(false)
                    .setPositiveButton(resources.getText(R.string.button_yes)) { _: DialogInterface?, _: Int ->
                        startProgress(true)
                        viewModel.logout()
                    }
                    .setNegativeButton(resources.getText(R.string.button_no)) { dialog: DialogInterface, _: Int -> dialog.cancel() }

                val alert = builder.create()

                alert.setOnShowListener { dialog: DialogInterface? ->
                    alert.getButton(AlertDialog.BUTTON_NEGATIVE)
                        .setTextColor(getColor(R.color.black))
                    alert.getButton(AlertDialog.BUTTON_POSITIVE)
                        .setTextColor(getColor(R.color.black))
                }

                alert.show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun startProgress(show: Boolean){
        if (show) progressDialog.show()
        else progressDialog.dismiss()
    }

    private fun gotoLogin()
    {
        val i = Intent(App.appContext, LoginActivity::class.java)
        startActivity(i)
        finish()
    }

    private fun showErrorMessage(errorMessage: String)
    {
        AwesomeDialog.build(this)
            .title(getString(R.string.title_warning))
            .body(errorMessage)
            .icon(R.drawable.ic_error)
            .onPositive(getString(R.string.button_Ok)) {}
    }

    private fun observeViewModel() {
        ColorDataController.INSTANCE.backgroundColorData.observe(this, { selectedColor ->
            viewModel.selectedColor = selectedColor
            binding.root.setBackgroundColor(selectedColor)
            binding.colorPicker.setInitialColor(viewModel.selectedColor)
        })

        viewModel.logoutResultObservable.observe(this, { result ->
            startProgress(false)
            if (result)
                gotoLogin()
            else
                showErrorMessage(getString(R.string.error_logout_error))
        })

        viewModel.process.observe(this, { processResult ->
            when (processResult.state) {
                LoadColorProcessStates.START -> startProgress(true)
                LoadColorProcessStates.ERROR -> {
                    startProgress(false)
                    showErrorMessage(processResult.message)
                }
                else -> startProgress(false)
            }
        })
    }
}