package com.fsecure.colorizer

import android.app.Application
import android.content.Context
import com.pixplicity.easyprefs.library.Prefs

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext

        Prefs.Builder()
            .setContext(this)
            .setMode(MODE_PRIVATE)
            .setPrefsName(packageName)
            .setUseDefaultSharedPreference(true)
            .build()
    }

    companion object {
        var appContext: Context? = null
    }
}