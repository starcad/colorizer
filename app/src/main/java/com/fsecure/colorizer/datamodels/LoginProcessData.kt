package com.fsecure.colorizer.datamodels

enum class LoginProcessStates{
    START, ERROR, DONE
}

data class LoginProcessData(val state: LoginProcessStates, val message: String) {
    constructor(state: LoginProcessStates) : this(state, "")
}