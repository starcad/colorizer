package com.fsecure.colorizer.datamodels

enum class  LoadColorProcessStates{
    START, ERROR, DONE
}

data class LoadColorProcessData(val state: LoadColorProcessStates, val message: String) {
    constructor(state: LoadColorProcessStates) : this(state, "")
}
