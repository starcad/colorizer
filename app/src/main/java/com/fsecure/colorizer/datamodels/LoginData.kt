package com.fsecure.colorizer.datamodels

import com.google.gson.annotations.SerializedName

data class LoginDataOut(val username: String, val password: String)

data class LoginTokenDataIn(
    @SerializedName("token") val token : String
)