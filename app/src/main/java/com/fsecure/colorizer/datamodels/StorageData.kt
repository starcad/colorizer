package com.fsecure.colorizer.datamodels

import com.google.gson.annotations.SerializedName

data class StorageColorDataOut(val data: Int)

data class StorageDataIn(
    @SerializedName("id") val id : String,
    @SerializedName("data") val data : Int,
)

