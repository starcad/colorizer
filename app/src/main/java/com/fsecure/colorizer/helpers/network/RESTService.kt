package com.fsecure.colorizer.helpers.network

import com.fsecure.colorizer.models.AppSettings
import com.fsecure.colorizer.BuildConfig
import okhttp3.ConnectionPool
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Singleton class for REST
 */
enum class RESTService {
    INSTANCE;

    private var mRetrofit: Retrofit? = null
    private val TIMEOUT:Long = 120

    fun Init() {
        val interceptor = HttpLoggingInterceptor()

        interceptor.level =
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

        val okHttpClient: OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .readTimeout(TIMEOUT, TimeUnit.SECONDS)
            .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            .connectionPool(ConnectionPool(0, 5, TimeUnit.MINUTES))
            .build()


        mRetrofit = Retrofit.Builder()
            .baseUrl(AppSettings.SERVER_URL)
            .addConverterFactory(NullOnEmptyConverterFactory())
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
    }

    val fsecureApi: FsecureTestAppAPI?
        get() {
            if (mRetrofit == null) Init()
            return mRetrofit?.create(FsecureTestAppAPI::class.java)
        }

    fun reinitRetrofit() {
        mRetrofit = null
    }
}