package com.fsecure.colorizer.helpers

import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import com.pixplicity.easyprefs.library.Prefs
import java.security.KeyStore
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.spec.GCMParameterSpec

/**
 * Singleton class for working with AES encryption
 */
enum class SecurityHelper {
    INSTANCE;

    lateinit var keygen:KeyGenerator

    private val TRANSFORMATION = "AES/GCM/NoPadding"
    private val KEYSTORENAME = "FSecureKeyStore2"

    private fun generateKey()
    {
        keygen = KeyGenerator
            .getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

        val keyGenParameterSpec = KeyGenParameterSpec.Builder(
            KEYSTORENAME,
                KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
        )
        .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
        .build()

        keygen.init(keyGenParameterSpec)
        keygen.generateKey();
    }

    private fun getSecretKey():SecretKey
    {
        val keyStore = KeyStore.getInstance("AndroidKeyStore")
        keyStore.load(null)
        val secretKeyEntry = keyStore
            .getEntry(KEYSTORENAME, null) as KeyStore.SecretKeyEntry

       return  secretKeyEntry.secretKey
    }

    fun encrypt(cryptingString: String):String
    {
        val cipher = Cipher.getInstance(TRANSFORMATION)

        generateKey()

        cipher.init(Cipher.ENCRYPT_MODE, getSecretKey())

        Prefs.putString("FS_SALT", Base64.encodeToString(cipher.iv, Base64.DEFAULT))

        val encryption = cipher.doFinal(cryptingString.toByteArray());

        return Base64.encodeToString(encryption, Base64.DEFAULT)
    }

    fun decrypt(encryptedData: String):String
    {
        val ivvStr = Prefs.getString("FS_SALT", "")
        var decryptedString = ""
        if(ivvStr.isNotEmpty())
        {
            val ivvv = Base64.decode(ivvStr,Base64.DEFAULT)
            val cipher = Cipher.getInstance(TRANSFORMATION)
            val d = Base64.decode(encryptedData, Base64.DEFAULT)
            val spec = GCMParameterSpec(128, ivvv)
            cipher.init(Cipher.DECRYPT_MODE, getSecretKey(), spec)
            val decodedData = cipher.doFinal(d)
            decryptedString = String(decodedData)
        }

        return decryptedString
    }
}