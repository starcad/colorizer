package com.fsecure.colorizer.helpers.network

import com.fsecure.colorizer.datamodels.*
import retrofit2.Response
import retrofit2.http.*

interface FsecureTestAppAPI {

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("v1/login")
    suspend fun checkLogin2(@Body data: LoginDataOut?): Response<LoginTokenDataIn>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("v1/storage")
    suspend fun createStorageAndSaveColor(@Header("Authorization") token: String, @Body data: StorageColorDataOut?): Response<StorageDataIn?>?

    @Headers("Content-Type: application/json", "Accept: application/json")
    @PUT("v1/storage/{id}")
    suspend fun saveColor(@Header("Authorization") token: String, @Path("id") id: String, @Body data: StorageColorDataOut?): Response<StorageDataIn?>?

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("v1/storage/{id}")
    suspend fun getColor(@Header("Authorization") token: String, @Path("id") id: String): Response<StorageDataIn?>?

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("v1/storage/{id}")
    suspend fun deleteStorage(@Header("Authorization") token: String, @Path("id") id: String): Response<Unit>
}