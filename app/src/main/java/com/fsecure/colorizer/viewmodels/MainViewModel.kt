package com.fsecure.colorizer.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fsecure.colorizer.App
import com.fsecure.colorizer.R
import com.fsecure.colorizer.datamodels.LoadColorProcessData
import com.fsecure.colorizer.datamodels.LoadColorProcessStates
import com.fsecure.colorizer.datamodels.StorageColorDataOut
import com.fsecure.colorizer.helpers.network.RESTService
import com.fsecure.colorizer.models.AppSettings
import com.fsecure.colorizer.models.LoginRepository
import com.fsecure.colorizer.models.MainRepository
import kotlinx.coroutines.*

class MainViewModel : ViewModel()
{
    private val storageRepository: MainRepository = MainRepository()
    private val loginRepository: LoginRepository = LoginRepository()
    val logoutResultObservable: MutableLiveData<Boolean> = MutableLiveData<Boolean>()

    val process: MutableLiveData<LoadColorProcessData> = MutableLiveData<LoadColorProcessData>()

    var selectedColor: Int = AppSettings.DEFAULT_COLOR

    var job: Job? = null
    var logoutJob: Job? = null

    fun saveColor()
    {
        if(storageRepository.getStorageId().isNotEmpty())
        {
            saveColorOrdinary()
        }
        else
        {
            saveColorFirstTime()
        }
    }

    /**
     * This is coroutine for REST which saving of color data firstly. With storage generating on backend.
     */
    private fun saveColorFirstTime()
    {
        process.postValue(LoadColorProcessData(LoadColorProcessStates.START))

        job = CoroutineScope(Dispatchers.IO).launch {
            val response = RESTService.INSTANCE.fsecureApi?.createStorageAndSaveColor(
                loginRepository.getToken(),
                StorageColorDataOut(selectedColor)
            )
            withContext(Dispatchers.Main) {
                if (response != null) {
                    if (response.isSuccessful) {
                        val storageId = response.body()?.id
                        if (storageId != null) {
                            storageRepository.rememberStorageId(storageId)
                            process.postValue(LoadColorProcessData(LoadColorProcessStates.DONE))
                        }
                        else
                        {
                            process.postValue(App.appContext?.let {
                                LoadColorProcessData(LoadColorProcessStates.ERROR, it.getString(
                                    R.string.error_storage_id_mustbe_not_null))
                            })
                        }
                    } else {
                        val errorText = if(response.message().isNotEmpty()) response.message() else response.errorBody()?.charStream()
                            ?.readText()
                        process.postValue(errorText?.let {
                            LoadColorProcessData(LoadColorProcessStates.ERROR,
                                it
                            )
                        })
                    }
                }
            }
        }
    }

    /**
     * This is coroutine for REST which saving of color data ordinary with using of current storage.
     */
    private fun saveColorOrdinary()
    {
        process.postValue(LoadColorProcessData(LoadColorProcessStates.START))

        job = CoroutineScope(Dispatchers.IO).launch {
            val response = RESTService.INSTANCE.fsecureApi?.saveColor(
                loginRepository.getToken(),
                storageRepository.getStorageId(),
                StorageColorDataOut(selectedColor)
            )
            withContext(Dispatchers.Main) {
                if (response != null) {
                    if (response.isSuccessful) {
                        process.postValue(LoadColorProcessData(LoadColorProcessStates.DONE))
                    } else {
                        val errorText = if(response.message().isNotEmpty()) response.message() else response.errorBody()?.charStream()
                            ?.readText()
                        process.postValue(errorText?.let {
                            LoadColorProcessData(LoadColorProcessStates.ERROR, it )
                        })
                    }
                }
            }
        }
    }

    fun logout()
    {
        logoutJob = CoroutineScope(Dispatchers.IO).launch {
            val response = RESTService.INSTANCE.fsecureApi?.deleteStorage(
                    loginRepository.getToken(),
                    storageRepository.getStorageId()
            )
            withContext(Dispatchers.Main) {
                if (response != null) {
                    if (response.isSuccessful) {
                        storageRepository.clearDb()
                        selectedColor = AppSettings.DEFAULT_COLOR
                        logoutResultObservable.postValue(true)
                    } else {
                        logoutResultObservable.postValue(false)
                    }
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
        logoutJob?.cancel()
    }
}