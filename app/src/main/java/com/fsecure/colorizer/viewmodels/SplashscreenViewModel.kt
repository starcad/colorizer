package com.fsecure.colorizer.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fsecure.colorizer.datamodels.LoadColorProcessData
import com.fsecure.colorizer.datamodels.LoadColorProcessStates
import com.fsecure.colorizer.helpers.network.RESTService
import com.fsecure.colorizer.models.ColorDataController
import com.fsecure.colorizer.models.LoginRepository
import com.fsecure.colorizer.models.MainRepository
import kotlinx.coroutines.*

class SplashscreenViewModel : ViewModel()
{
    private val loginRepository: LoginRepository = LoginRepository()
    private val storageRepository: MainRepository = MainRepository()

    val process: MutableLiveData<LoadColorProcessData> = MutableLiveData<LoadColorProcessData>()

    var job: Job? = null

    fun isAlreadyAuth():Boolean
    {
        return loginRepository.getToken().isNotEmpty()
    }

    fun isAlreadyHaveStorage():Boolean
    {
        return storageRepository.getStorageId().isNotEmpty()
    }

    fun getColorFromStorage()
    {
        process.postValue(LoadColorProcessData(LoadColorProcessStates.START))

        job = CoroutineScope(Dispatchers.IO).launch {
            val response = RESTService.INSTANCE.fsecureApi?.getColor(
                loginRepository.getToken(),
                storageRepository.getStorageId()
            )
            withContext(Dispatchers.Main) {
                if (response != null) {
                    if (response.isSuccessful) {
                        val colorFromStorage = response.body()?.data
                        ColorDataController.INSTANCE.backgroundColorData.postValue(colorFromStorage)
                        process.postValue(LoadColorProcessData(LoadColorProcessStates.DONE))
                    } else {
                        val errorText = if(response.message().isNotEmpty()) response.message() else response.errorBody()?.charStream()
                            ?.readText()
                        process.postValue(errorText?.let { LoadColorProcessData(LoadColorProcessStates.ERROR, it) })
                    }
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}

