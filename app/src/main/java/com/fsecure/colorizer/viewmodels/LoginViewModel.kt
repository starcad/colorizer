package com.fsecure.colorizer.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fsecure.colorizer.App
import com.fsecure.colorizer.R
import com.fsecure.colorizer.datamodels.LoginDataOut
import com.fsecure.colorizer.datamodels.LoginProcessData
import com.fsecure.colorizer.datamodels.LoginProcessStates
import com.fsecure.colorizer.helpers.network.RESTService
import com.fsecure.colorizer.models.LoginRepository
import kotlinx.coroutines.*

class LoginViewModel : ViewModel()
{
    private val loginRepository: LoginRepository = LoginRepository()

    /**
     * Mutable live data for login process ruling
     */
    val loginProcessDataObservable: MutableLiveData<LoginProcessData> = MutableLiveData<LoginProcessData>()
    var logData: LoginDataOut? = null

    var job: Job? = null

    fun login() {
        loginProcessDataObservable.postValue(LoginProcessData(LoginProcessStates.START))

        job = CoroutineScope(Dispatchers.IO).launch {
            val response = RESTService.INSTANCE.fsecureApi?.checkLogin2(logData)
            withContext(Dispatchers.Main) {
                if (response != null) {
                    if (response.isSuccessful) {
                        val token = response.body()?.token
                        if (token != null) {
                            loginRepository.rememberToken(token)
                            loginProcessDataObservable.postValue(LoginProcessData(LoginProcessStates.DONE))
                        }
                        else
                        {
                            loginProcessDataObservable.postValue(App.appContext?.let { LoginProcessData(LoginProcessStates.ERROR, it.getString(R.string.error_on_server)) })
                        }
                    } else {
                        val errorText = if(response.message().isNotEmpty()) response.message() else response.errorBody()?.charStream()
                            ?.readText()
                        loginProcessDataObservable.postValue(errorText?.let { LoginProcessData(LoginProcessStates.ERROR, it ) })
                    }
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}

