package com.fsecure.colorizer.models

import androidx.lifecycle.MutableLiveData

/**
 * Singleton model for global accessable background color.
 */
enum class ColorDataController {
    INSTANCE;

    var backgroundColorData = MutableLiveData<Int>()
}