package com.fsecure.colorizer.models

/**
 * Constants for App
 */
object AppSettings {
    const val SERVER_URL = "https://54t9f06ot1.execute-api.eu-central-1.amazonaws.com/api/"
    const val DEFAULT_COLOR = 0xFFFFFF
}