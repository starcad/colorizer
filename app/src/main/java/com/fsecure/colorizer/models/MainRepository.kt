package com.fsecure.colorizer.models

import com.pixplicity.easyprefs.library.Prefs

/**
 * Repository for storage data
 */
class MainRepository
{
    private val STORAGE_ID_KEY = "FS_STID"

    fun rememberStorageId(id: String)
    {
        Prefs.putString(STORAGE_ID_KEY, id)
    }

    fun getStorageId(): String
    {
        return Prefs.getString(STORAGE_ID_KEY, "")
    }

    fun clearDb()
    {
       Prefs.clear()
    }
}