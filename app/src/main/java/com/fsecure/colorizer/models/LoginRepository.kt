package com.fsecure.colorizer.models

import com.fsecure.colorizer.helpers.SecurityHelper
import com.pixplicity.easyprefs.library.Prefs

/**
 * Repository for login data aka token
 */
class LoginRepository {

    private val TOKEN_KEY = "FS_TOKEN"

    fun rememberToken(token:String) {
        val secToken = SecurityHelper.INSTANCE.encrypt(token)
        Prefs.putString(TOKEN_KEY, secToken)
    }

    fun getToken():String
    {
        var _token = ""
        val token = Prefs.getString(TOKEN_KEY, "")
        if (token.isNotEmpty()) {
            _token = SecurityHelper.INSTANCE.decrypt(token)
        }
        return _token
    }

    fun clearToken()
    {
        Prefs.remove(TOKEN_KEY)
    }
}